<?php

/*
 * You must implement hook_migrate_api(), setting the API level to 2, if you are
 * implementing any migration classes. As of Migrate 2.5, you should also
 * register your migration and handler classes explicitly here - the former
 * method of letting them get automatically registered on a cache clear will
 * break in certain environments (see http://drupal.org/node/1778952).
 */

function menumigrate_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'menumigrate' => array(
        'title' => t('Menu migrate'),
      ),
    ),
    'migrations' => array(
      'MenuToNode' => array(
        'class_name' => 'MenuToNodeMigration',
        'group_name' => 'menumigrate',
      ),
      'MenuToNodeSpread' => array(
        'class_name' => 'MenuToNodeSpreadMigration',
        'group_name' => 'menumigrate',
      ),
    ),
  );
  return $api;
}
