<?php

/**
 * Migrate a site's placeholder menu items into nodes.
 */
class MenuToNodeMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = 'Create nodes from menu items (on the same Drupal site).';

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'mlid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Menu Link ID.',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $query = db_select('menu_links', 'ml')
             ->fields('ml', array('mlid', 'link_title', 'options'))
             ->where("link_path='" . variable_get('menu_import_fallback_link_path', '<front>') . "'");
    $this->source = new MigrateSourceSQL($query);

    $this->destination = new MigrateDestinationNode('main_article', array('text_format' => 'full_html'));

    $this->addFieldMapping('title', 'link_title');

    // Defaults
    $uid = variable_get('migratemenu_uid', 0);
    $uid = $uid ? $uid : ($GLOBALS['user']->uid ? $GLOBALS['user']->uid : 1);
    $this->addFieldMapping('uid')->defaultValue($uid)->description('Assign authorship to value of migratemenu_uid if set or fall back to present user or (for Drush) user 1.');
    $this->addFieldMapping('field_type_of_content')->defaultValue('news')->description('News sub-type.');
    $this->addFieldMapping('field_can_be_shared')->defaultValue(TRUE)->description('Article can be shared.');
    if (variable_get('migratemenu_og_group_article_ref', '')) {
      $this->addFieldMapping('og_group_ref')->defaultValue(variable_get('migratemenu_og_group_article_ref', ''))->description('The organic group to which spreads should be added.');
    }
    else {
      drupal_set_message(t('Variable <em>migratemenu_og_group_ref</em> was not set, imported main article nodes not associated with an organic group.'), 'warning');
    }

    $this->addFieldMapping('format')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('weight')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
         ->issueGroup(t('DNM'));

    // We conditionally DNM these fields, so your field mappings will be clean
    // whether or not you have path and or pathauto enabled
    if (module_exists('path')) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array('created', 'changed', 'status',
      'promote', 'revision', 'language', 'revision_uid', 'log', 'tnid',
      'body:format', 'body:language'));

  }
}


/**
 * Spreads for news migration.
 */
class MenuToNodeSpreadMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Create Spread nodes for each menu item previously imported as articles.');

    $this->dependencies = array('MenuToNode');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'mlid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Menu Link ID.',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
    $query = db_select('menu_links', 'ml')
             ->fields('ml', array('mlid', 'link_title', 'options'))
             ->where("link_path='" . variable_get('menu_import_fallback_link_path', '<front>') . "'");
    $this->source = new MigrateSourceSQL($query);

    $this->destination = new MigrateDestinationNode('spread');

    $this->addFieldMapping('title', 'link_title');
    $this->addFieldMapping('field_referenced_article', 'mlid')->sourceMigration('MenuToNode');

    // Defaults
    $uid = variable_get('migratemenu_uid', 0);
    $uid = $uid ? $uid : ($GLOBALS['user']->uid ? $GLOBALS['user']->uid : 1);
    $this->addFieldMapping('uid')->defaultValue($uid)->description('Assign authorship to value of migratemenu_uid if set or fall back to present user or (for Drush) user 1.');
    $this->addFieldMapping('field_content_topic')->defaultValue('Global Health')->description('Global Health category.');
    if (variable_get('migratemenu_og_group_ref', '')) {
      $this->addFieldMapping('og_group_ref')->defaultValue(variable_get('migratemenu_og_group_ref', ''))->description('The organic group to which spreads should be added.');
    }
    else {
      drupal_set_message(t('Variable <em>migratemenu_og_group_ref</em> was not set, imported spread nodes not associated with an organic group.'), 'error');
    }

    $this->addFieldMapping('format')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('weight')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
         ->issueGroup(t('DNM'));

    // We conditionally DNM these fields, so your field mappings will be clean
    // whether or not you have path and or pathauto enabled
    if (module_exists('path')) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }

  /**
   * Update menu links with spread node path.
   */
  public function complete($entity, $row) {
    // Note: Our own query would be lighter than menu_link_load().
    $item = menu_link_load($row->mlid);
    $item['link_path'] = 'node/' . $entity->nid;
    menu_link_save($item);
  }
}
